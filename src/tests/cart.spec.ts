import {expect, test} from '@playwright/test'
import { LocationPage } from '../pages/LocationPage';

test.describe('add to cart function test', ()=>{
    
    //scenario 2
    test.use({
        permissions: ['geolocation'],
        geolocation: {
          latitude: -6.3004608,
          longitude: 106.6486423,
        },
    });

    test.beforeEach(async ({page, baseURL}) => {
        await page.setViewportSize({width:1920,height:1280});
        if (!baseURL) throw 'baseURL empty';
        //scenario 1
        await page.goto(baseURL);
        await page.waitForURL('**\/locations');
        await expect(page).toHaveURL(/.*locations/);
    });


    test('add item multiple times to cart then reduce it in cart', async({page})=>{
        let count=0;
        //scenario 3
        const itemListPage = await new LocationPage(page).selectTopMostLocation();
        const product = await itemListPage.getFirstProductLocator();
        //scenario 4
        const name = await itemListPage.getProductNameByProductLocator(product);
        const price = await itemListPage.getProductPriceByProductLocator(product);
        const priceInNumber = Number(price.replace('.',''));
        await itemListPage.clickAddByProductLocator(product);
        await itemListPage.clickPlusByProductLocator(product);
        const floatingTextLocator = await itemListPage.getFloatingCart();
        count = Number(await itemListPage.getProductCountByProductLocator(product));
        const totalPrice = (priceInNumber*count).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        //scenario 5
        await expect(floatingTextLocator).toHaveText('Total '+totalPrice+' for '+count+' products',{timeout: 10000});

        const cartPage = await itemListPage.clickFloatingCart();
        //scenario 6
        const productCart = await cartPage.getProductLocatorByName(name);
        const priceCart = await cartPage.getProductPriceByProductLocator(productCart);
        expect(price).toEqual(priceCart);


        //scenario 7
        const countBefore = Number(await cartPage.getProductCountByProductLocator(productCart));
        await cartPage.clickMinusByProductLocator(productCart);
        count = Number(await cartPage.getProductCountByProductLocator(productCart));
        expect(countBefore).toEqual(count+1);

        const productDetailPage = await cartPage.openProductDetail(productCart);
        const nameDetail = await productDetailPage.getName();
        const priceDetail = await productDetailPage.getPrice();
        const popUp = await productDetailPage.getPopUpLocator();

        expect(name).toEqual(nameDetail);
        expect(price).toEqual(priceDetail);
        await expect(popUp).toBeVisible();
        
    
    })
})