import BasePage from "./BasePage";
import { ProductPage } from ".";
export class LocationPage extends BasePage{
    async selectTopMostLocation():Promise<ProductPage>{
        await this.page.locator('.css-10hhgxr').first().click();
        return new ProductPage(this.page);
    }
}