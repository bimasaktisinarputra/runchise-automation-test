import { Locator } from "@playwright/test";
import BasePage from "./BasePage";
import { ProductDetailPage } from "./ProductDetailPage";
export class CartPage extends BasePage{
    async getFirstProductLocator():Promise<Locator>{
        const firstItem = this.page.locator('.css-56jipt').first();
        return(firstItem);
    }

    async getProductLocatorByName(name:string):Promise<Locator>{
        const nameLocator = this.page.getByText(name);
        const item = nameLocator.locator('..').locator('..').locator('..').locator('..');
        return item;
    }

    async getProductNameByProductLocator(item:Locator):Promise<string>{
        const name = await item.locator('.css-anr7ky').textContent()||'';
        return name;
    }

    async getProductPriceByProductLocator(item:Locator):Promise<string>{
        const price = await item.locator('.css-105twdj').textContent()||'';
        return price;
    }

    async getProductCountByProductLocator(item:Locator):Promise<string>{
        const count = await item.locator('.css-1hy4xsj').textContent()||'';
        return count;
    }

    async clickPlusByProductLocator(item:Locator):Promise<CartPage>{
        await item.locator('.css-sjx0zd').nth(1).click();
        return this;
    }

    async clickMinusByProductLocator(item:Locator):Promise<CartPage>{
        await item.locator('.css-sjx0zd').first().click();
        return this;
    }

    async openProductDetail(item:Locator):Promise<ProductDetailPage>{
        await this.page.waitForLoadState('load');
        await item.click();
        return new ProductDetailPage(this.page);
    }
}