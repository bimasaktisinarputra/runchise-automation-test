import { Locator } from "@playwright/test";
import BasePage from "./BasePage";
import { CartPage } from "./CartPage";
export class ProductPage extends BasePage{
    async getFirstProductLocator():Promise<Locator>{
        const firstItem = this.page.locator('.css-1ne0r2x').first();
        return(firstItem);
    }

    async getProductNameByProductLocator(item:Locator):Promise<string>{
        const name = await item.locator('.css-anr7ky').textContent()||'';
        return name;
    }

    async getProductPriceByProductLocator(item:Locator):Promise<string>{
        const price = await item.locator('.css-105twdj').textContent()||'';
        return price;
    }

    async getProductCountByProductLocator(item:Locator):Promise<string>{
        const count = await item.locator('.css-1hy4xsj').textContent()||'';
        return count;
    }
    
    async clickAddByProductLocator(item:Locator):Promise<ProductPage>{
        await item.getByText('Add').click();
        return this;
    }

    async clickPlusByProductLocator(item:Locator):Promise<ProductPage>{
        await item.locator('.css-sjx0zd').nth(1).click();
        return this;
    }

    async getFloatingCart():Promise<Locator>{
        return this.page.locator('.css-q0pmnr').locator('.css-oxu4bc');
    }

    async clickFloatingCart():Promise<CartPage>{
        await this.page.locator('.css-1xe5izx').click();
        await this.page.waitForURL('/product/cart');
        return new CartPage(this.page);
    }
}