import { Locator } from "@playwright/test";
import BasePage from "./BasePage";
export class ProductDetailPage extends BasePage{
    
    async getName():Promise<string>{
        const name = await this.page.locator('.css-rghnp1').textContent()||'';
        return name;
    }

    async getPrice():Promise<string>{
        const price = await this.page.locator('.css-105twdj').textContent()||'';
        return price;
    }

    async getPopUpLocator():Promise<Locator>{
        const popUp = this.page.locator('.css-1ghmqwf');
        return popUp;
    }
}